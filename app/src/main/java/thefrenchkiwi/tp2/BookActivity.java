package thefrenchkiwi.tp2;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class BookActivity  extends AppCompatActivity {
    private Intent mIntent;
    private Book mBook;
    private EditText mEditTitle, mEditAuthors, mEditYear, mEditPublisher, mEditGenres;
    private Button mButtonSave;
    private BookDbHelper mBookDbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book);

        mBookDbHelper = new BookDbHelper(this);

        mEditTitle = findViewById(R.id.nameBook);
        mEditAuthors = findViewById(R.id.editAuthors);
        mEditGenres = findViewById(R.id.editGenres);
        mEditYear = findViewById(R.id.editYear);
        mEditPublisher = findViewById(R.id.editPublisher);
        mButtonSave = findViewById(R.id.button);

        mIntent = getIntent();
        mBook = mIntent.getParcelableExtra("book_selected");

        if(mBook!=null)
            fillMyFields(mBook);

        mButtonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mBook == null) {
                    mBook = new Book();
                    updateMyBook(mBook);
                    if (mBook.getTitle().equals("")) {
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(BookActivity.this);
                        builder1.setMessage("There is no title! ");
                        builder1.setCancelable(true);
                        builder1.show();
                        return;
                    }


                    if (!mBookDbHelper.addBook(mBook)) {
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(BookActivity.this);
                        builder1.setMessage("This book already exists! ");
                        builder1.setCancelable(true);
                        builder1.show();
                        return;

                    }


                } else {
                    updateMyBook(mBook);
                    if (mBook.getTitle().equals("")) {
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(BookActivity.this);
                        builder1.setMessage("No title! ");
                        builder1.setCancelable(true);
                        builder1.show();
                        return;
                    }
                    mBookDbHelper.updateBook(mBook);
                    Context context = getApplicationContext();
                    CharSequence text = "Updated the book";
                    int duration = Toast.LENGTH_SHORT;

                    Toast toast = Toast.makeText(context, text, duration);
                    toast.show();
                    return;

                }
                finish();
            }
        });

    }

    private void fillMyFields(Book mBook) {
        mEditTitle.setText(mBook.getTitle());
        mEditAuthors.setText(mBook.getAuthors());
        mEditYear.setText(mBook.getYear());
        mEditGenres.setText(mBook.getGenres());
        mEditPublisher.setText(mBook.getPublisher());

    }

    private void updateMyBook(Book mBook){
        mBook.setTitle(mEditTitle.getText().toString());
        mBook.setAuthors(mEditAuthors.getText().toString());
        mBook.setYear(mEditYear.getText().toString());
        mBook.setGenres(mEditGenres.getText().toString());
        mBook.setPublisher(mEditPublisher.getText().toString());

    }


}
