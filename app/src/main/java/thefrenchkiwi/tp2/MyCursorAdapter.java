package thefrenchkiwi.tp2;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

public class MyCursorAdapter extends CursorAdapter {

    public MyCursorAdapter(Context context, Cursor cursor, int flags) {
        super(context, cursor, flags);


    }

    public void bindView(View view, Context context, Cursor cursor) {
        TextView textViewTitle = (TextView) view.findViewById(R.id.title_book);
        String title = cursor.getString( cursor.getColumnIndex( BookDbHelper.COLUMN_BOOK_TITLE ) );
        textViewTitle.setText(title);

        TextView textViewAuthors = (TextView) view.findViewById(R.id.authors_book);
        String authors = cursor.getString( cursor.getColumnIndex( BookDbHelper.COLUMN_AUTHORS ) );
        textViewAuthors.setText(authors);

    }
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        // R.layout.list_row is your xml layout for each row
        System.out.println("adding view");
        return LayoutInflater.from(context).inflate(R.layout.content_listview, parent, false);
    }


}
